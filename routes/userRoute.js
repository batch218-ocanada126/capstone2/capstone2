// dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

//start ofs42 capstone2
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.get("/details/:id", (req, res) => {
	userController.getUser(req.params.id).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});

//end of s42 capstone2


router.post("/checkout", auth.verify , (req, res) =>{
	const data = {
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.createOrder(data).then(resultFromController => res.send(resultFromController))
});


//start of Stretch Goal
//admin only
router.patch("/:userId/setAsAdmin", auth.verify, (req, res) =>{

	const data = {
		user: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.setUserAdmin(req.params.userId, data).then(resultFromController => res.send(resultFromController))
})


//adming only
router.get("/orders", auth.verify, (req, res) =>{
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});

router.get("/myOrders" , auth.verify, (req, res) =>{
	const data = {
		userId: auth.decode(req.headers.authorization).id
	}
	userController.getUserOrders(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;