

const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const userController = require("../controllers/userController.js");
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");
const Cart = require("../models/cart.js");


// Start of s43 capstone2
router.post("/createProduct", auth.verify, (req, res) =>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});


router.get("/activeProduct", (req, res) =>{
	productController.getActiveProduct().then(resultFromController => res.send(resultFromController));
});

// End of s43 capstone2


//Start of s44 capstone2



//get all product
router.get("/allProduct", (req, res) =>{

	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});



//all user
router.get("/:productId", (req, res) =>{
	console.log(req.params.productId);
	console.log(req.params);
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
});


//scanProduct
/*router.post("/scanProduct", (req, res) => {
    console.log(req.body)
    productController.scanProduct(req.body).then(resultFromController => res.send(resultFromController));
});*/

//admin only


/*router.patch("/:productId", auth.verify, (req, res) => {
		console.log(req.params.productId)
    const productId = req.params.productId;

    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).send({ message: "Invalid product ID" });
    }

    const newData = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }


    if (newData.isAdmin) {
        productController.updateProduct(productId, newData)
            .then(resultFromController => {
                if (resultFromController === false) {
                    return res.status(500).send({ message: 'An error occurred while updating the product' });
                }
                return res.send(resultFromController);
            })
            .catch(err => res.status(500).send({ message: err.message }));
    } else {
        return res.status(401).send({ message: "Unauthorized" });
    }
});*/





router.patch("/:productId",(req, res) =>{

	console.log()
	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
});





//admin only

router.put("/:productId/archive", auth.verify, (req, res) => {
	console.log(req.body);

  const newData = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  productController.archiveProduct(req.params.productId, newData)
    .then(resultFromController => {
      if (resultFromController) {
        res.status(200).send({ message: "Product archived successfully" });
      } else {
        res.status(404).send({ message: "Error archiving product" });
      }
    })
   
});



router.put("/:productId/activate", auth.verify, (req, res) => {
	console.log(req.body);

  const newData = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  productController.activateProduct(req.params.productId, newData)
    .then(resultFromController => {
      if (resultFromController) {
        res.status(200).send({ message: "Product archived successfully" });
      } else {
        res.status(404).send({ message: "Error archiving product" });
      }
    })
   
});



/*router.put("/:productId/archive", auth.verify, (req, res) =>{


	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(req.params.productId, newData).then(resultFromController => res.send(resultFromController)
	);
});
*/

//End of s44 capstone2


// add to cart feature

router.patch("/addToCart", auth.verify, (req, res) => {
  const data = {
    userId: auth.decode(req.headers.authorization).id
  };

  productController.addedProducts(data, req.body)
    .then((resultFromController) => res.send(resultFromController));
});



module.exports = router;

