

const User = require("../models/user.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");


//start s42 capstone2
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({

		username: reqBody.username,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		isAdmin: reqBody.isAdmin,
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		return true;
	})


};

// Checking Email
module.exports.checkEmailExist = (reqBody) => {
	// ".find" - a mongoose crud operation (query) to find a field value from a collection
	return User.find({email: reqBody.email }).then(result => {
		// condition if there is an exsiting  user
		if(result.length > 0){
			return true;
		}
		// condition if there is no existing user
		else
		{
			return false;
		}
	})
};


module.exports.loginUser = (reqBody) =>{
	return User.findOne({username : reqBody.username}).then(result =>{
		if(result == null){
			return {
				alert : "Invalid username!"
			}
			
		}
		else{
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				//return false;
				return  {alert: "Invalid password"}
			}
		}
	})

};


module.exports.getUser = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = "";
			return details;
		}
	})
	.catch(error => {
      console.error(error);
      return false;
    });
};


	//Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		// console.log(data.userId);
		// console.log(result);
		
		if (result == null) {
			return false
		} else {
			result.password = "*****"

			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};

//end of s42 capstone2


module.exports.createOrder = (data) =>{
	if(data.isAdmin == false){

		let newOrder = new Order({
			products: [{
				productId: data.productId,
				quantity: data.quantity}],
				totalAmount: data.totalAmount,
				userId: data.userId
			
		})

		return newOrder.save().then((newOrder, error) =>{
			if(error){
				return error;
			}
			else{
				return newOrder;
			}
		})
	} 
	else{
		
		return true;
	}
}



module.exports.setUserAdmin = (userId, data) => {
	if(data.isAdmin == true){
		return User.findByIdAndUpdate(userId,
		{
			isAdmin: data.user.isAdmin
		}	

	).then((updatedUser, error)=>{
		if(error){
			return false;
		}
		return data;
	})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update User role')
		return message.then((value) => {return value})
	};
}


module.exports.getAllOrders = (data) =>{
	if(data.isAdmin == true){
	return Order.find().then(result =>{
		return result;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to get all orders')
		return message.then((value) => {return value})
	};
};


module.exports.getUserOrders = (data) =>{
	if(data.userId){
		return Order.find({userId: data.userId}).then(result =>{
			return result;
		})
	}
	else{
		return false;
	}
}

