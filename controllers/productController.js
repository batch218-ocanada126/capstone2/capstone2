

const mongoose = require("mongoose");
const Product = require("../models/product.js");
const Cart = require("../models/cart.js");


//start of s43 capstone2
module.exports.createProduct = (data) =>{
	console.log(data.isAdmin)


	if(data.isAdmin){
		let newProduct = new Product({
			title: data.product.title,
			description: data.product.description,
			//size: data.product.size,
			//color: data.product.color,
			price: data.product.price,
			isActive: data.product.isActive,
			productId: data.product.productId
		});

		return newProduct.save().then((newProduct, error) =>{
			if(error){
				return error;
			}

			return newProduct
		})

	};

	let message = Promise.resolve('User must be ADMIN to create new product')

	return message.then((value) =>{
		return {value}
	})

};


module.exports.getActiveProduct = () =>{
	return Product.find({isActive:true}).then(result =>{
		return result;
	})
};
//end of s43 capstone2




//Start of s44 capstone2


	module.exports.getAllProducts = () => {
    return Product.find({}).then(result => result)
}




// all Users
module.exports.getProduct = (productId) =>{
	
	console.log(productId)
	return Product.findById(productId).then(result =>{
		console.log(result)
		return result;
	});
};


//admin only

	module.exports.getAllProducts = () => {
    return Product.find({}).then(result => result)
}



	/*module.exports.scanProduct = (reqBody) => {
  return Product.find({title: reqBody.title}).then(result => {
          console.log(result)

          if(result.length > 0) {

              return  {result:result[0], exist:true};
          }
          else
          {
              return false;
          }
      })
  }*/

/*module.exports.updateProduct = (productId, newData) => {
  // Check if productId is a valid ObjectId
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return Promise.reject(new Error("Invalid productId"));
  }

  // Find and update the product
  return Product.findByIdAndUpdate(productId, newData)
    .then((updatedProduct) => {
      if (!updatedProduct) {
        return Promise.reject(new Error("Product not found"));
      }
      return updatedProduct;
    })
    .catch((err) => Promise.reject(err));
};*/



//update product
/*module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		//update code block
		return Product.findByIdAndUpdate(productId ,
		{
			title: newData.product.title,
			description: newData.product.description,
			
			price: newData.product.price
			

		}
		).then((updatedProduct, error) =>{
			if(error){
				return false;
			}
			return newData;
		})
	}
	else{
		return true;
	};
};*/

  module.exports.updateProduct = (productId, reqBody) => {

  			
       return Product.findByIdAndUpdate(productId ,
          {
        
              price: reqBody.price
          }

          ).then((updatedProduct, error) =>{
          	console.log(updatedProduct)
              if(error){
                  return false;
              }
              return true;
          })
  };




//admin only

 	module.exports.archiveProduct = (productId, reqBody) => {
    return Product.findByIdAndUpdate(productId, {
            isActive: false
        }
        ).then((updateProduct, err) => {
            if (err) {
                return false
            }
                return true
        })
}
		

		module.exports.activateProduct = (productId, reqBody) => {

		    console.log(productId)
		    return Product.findByIdAndUpdate(productId, {
		            isActive: true
		        }
		        ).then((updateProduct, err) => {
		            if (err) {
		                return false
		            }
		                return true
		        })
		}

		/*	module.exports.archiveProduct = (productId, newData) => {
				if(newData.isAdmin && newData.product){
					return Product.findByIdAndUpdate(productId , {
						isActive: newData.product.isActive
					}, {new: true} )
					.then((archivedProduct) =>{
						if(!archivedProduct){
							return false;
								}
						return archivedProduct;
						});
					}
				else{
					let message = Promise.resolve('User must be ADMIN to archive Product')
					return message.then((value) => {return value})
			};
		};


		module.exports.activateProduct = (productId, newData) => {
				if(newData.isAdmin && newData.product){
					return Product.findByIdAndUpdate(productId , {
						isActive: newData.product.isActive
					}, {new: true} )
					.then((archivedProduct) =>{
						if(!archivedProduct){
							return true;
								}
						return archivedProduct;
						});
					}
				else{
					let message = Promise.resolve('User must be ADMIN to archive Product')
					return message.then((value) => {return value})
			};
		};*/

/*module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId , {
				isActive: newData.product.isActive
			})

		.then((archivedProduct, error) =>{
			if(error){
				return false;
				}

			return {
				message : "Product archived successfully!"
			}		
		});
	}
	else{
		let message = Promise.resolve('User must be ADMIN to archive Product')
		return message.then((value) => {return value})
	};
};*/

//End of s44 capstone2


//Add to cart section

module.exports.addedProducts = (data, item) => {
  if (data.userId) {
    return Cart.findOneAndUpdate(
      { userId: data.userId },
      { $push: { products: item } },
      { upsert: true, new: true }
    ).then((addProducts, error) => {
      if (error) {
        return false;
      }
      return addProducts;
    });
  } 
};
